%global debug_package %{nil}

# Turn off the brp-python-bytecompile script
%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')

Name: puppet-agent
Version: 1.10.9
Release: 1%{?dist}
Summary: The Puppet Agent package contains all of the elements needed to run puppet, including ruby, facter, hiera and mcollective.
Vendor: Puppet Labs
License: See components
Group: System Environment/Base
URL: https://www.puppetlabs.com

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Source0: http://sat-r220-02.lab.eng.rdu2.redhat.com/pulp/isos/Sat6-CI-Source_Files-Satellite_6_3_Source_Files/%{name}-%{version}.tar.gz
Source1: file-list-for-rpm

ExcludeArch: ppc ppc64 s390 s390x ia64 aarch64

# Don't provide anything so the system doesn't use our packages to resolve deps
Autoprov: 0
Autoreq: 0
Requires: tar
Requires: readline
Requires: util-linux

# All rpm packages built by vanagon have the pre-/post-install script
# boilerplates (defined below). These require `mkdir` and `touch` but previously
# did not specify a dependency on these.
# In the future, we will supress pre/post scripts completely if there's nothing
# specified by the project or the components.

%if 0%{?rhel} >= 7
BuildRequires: systemd
Requires(post):   systemd
Requires(preun):  systemd
Requires(postun): systemd
%else
Requires: chkconfig
%endif

Obsoletes: puppet < 4.0.0
Obsoletes: pe-puppet-enterprise-release < 4.0.0
Obsoletes: pe-puppetserver-common < 4.0.0
Obsoletes: facter < 1:3.0.0
Obsoletes: cfacter < 0.5.0
Obsoletes: hiera < 2.0.0
Obsoletes: mcollective < 3.0.0
Obsoletes: mcollective-common < 3.0.0
Obsoletes: mcollective-client < 3.0.0

Conflicts: pe-r10k < 2.5.0.0

Provides: puppet >= 4.0.0
Provides: facter >= 1:3.0.0
Provides: cfacter >= 0.5.0
Provides: hiera >= 2.0.0
Provides: mcollective >= 3.0.0
Provides: mcollective-common >= 3.0.0
Provides: mcollective-client >= 3.0.0

%description
The Puppet Agent package contains all of the elements needed to run puppet, including ruby, facter, hiera and mcollective.

Contains the following components:
augeas 1.4.0
cpp-hocon 0.1.4
cpp-pcp-client 1.5.4
curl 7.51.0
dmidecode 2.12
facter 3.6.7
hiera 3.3.2
leatherman 0.12.2
libxml2 2.9.4
libxslt 1.1.29
marionette-collective 2.10.5
openssl 1.0.2k
puppet 4.10.7
puppet-ca-bundle 1.0.7
pxp-agent 1.5.5
ruby-2.1.9 2.1.9
ruby-augeas 0.5.0
ruby-selinux 2.0.94
ruby-shadow 2.3.3
ruby-stomp 1.3.3
rubygem-deep-merge 1.0.1
rubygem-fast_gettext 1.1.0
rubygem-gettext 3.2.2
rubygem-gettext-setup 0.20
rubygem-hocon 1.2.5
rubygem-locale 2.1.2
rubygem-net-ssh 4.1.0
rubygem-semantic_puppet 0.1.2
rubygem-text 1.3.1
runtime
shellpath 2015-09-18
virt-what 1.14

%prep
%setup -q -n %{name}-%{version}

if [ %{_arch} = 'ppc64le' ]; then
  sed -i 's/x86_64-linux/powerpc64le-linux/g' %{SOURCE1}
  sed -i '/\/opt\/puppetlabs\/puppet\/bin\/catstomp/d' %{SOURCE1}
  sed -i '/\/opt\/puppetlabs\/puppet\/bin\/hocon/d' %{SOURCE1}
  sed -i '/\/opt\/puppetlabs\/puppet\/bin\/rmsgcat/d' %{SOURCE1}
  sed -i '/\/opt\/puppetlabs\/puppet\/bin\/rmsgfmt/d' %{SOURCE1}
  sed -i '/\/opt\/puppetlabs\/puppet\/bin\/rmsginit/d' %{SOURCE1}
  sed -i '/\/opt\/puppetlabs\/puppet\/bin\/rmsgmerge/d' %{SOURCE1}
  sed -i '/\/opt\/puppetlabs\/puppet\/bin\/rxgettext/d' %{SOURCE1}
  sed -i '/\/opt\/puppetlabs\/puppet\/bin\/stompcat/d' %{SOURCE1}
  sed -i '/\/opt\/puppetlabs\/puppet\/lib\/ruby\/vendor_ruby\/facter.jar/d' %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/libgcc_s.so" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/libgcc_s.so.1" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/libssp.so" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/libssp.so.0" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/libssp.so.0.0.0" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/libstdc++.so" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/libstdc++.so.6" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/libstdc++.so.6.0.22" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/bin/catstomp" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/bin/hocon" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/bin/rmsgcat" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/bin/rmsgfmt" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/bin/rmsginit" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/bin/rmsgmerge" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/bin/rxgettext" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/bin/stompcat" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/share/doc/rbconfig.rb" >> %{SOURCE1}
  sed -i "/\/opt\/puppetlabs\/puppet\/bin\/dmidecode/d" %{SOURCE1}
  sed -i "/\/opt\/puppetlabs\/puppet\/share\/doc\/dmidecode/d" %{SOURCE1}
  sed -i "/\/opt\/puppetlabs\/puppet\/share\/doc\/dmidecode\/AUTHORS/d" %{SOURCE1}
  sed -i "/\/opt\/puppetlabs\/puppet\/share\/doc\/dmidecode\/CHANGELOG/d" %{SOURCE1}
  sed -i "/\/opt\/puppetlabs\/puppet\/share\/doc\/dmidecode\/README/d" %{SOURCE1}
  sed -i "/\/opt\/puppetlabs\/puppet\/share\/man\/man8\/dmidecode.8/d" %{SOURCE1}
elif [ %{_arch} = 'i386' ]; then
  sed -i 's/x86_64-linux/i686-linux/g' %{SOURCE1}
elif [ %{_arch} = 'aarch64' ]; then
  sed -i 's/x86_64-linux/aarch64-linux/g' %{SOURCE1}
  sed -i '/\/opt\/puppetlabs\/puppet\/bin\/catstomp/d' %{SOURCE1}
  sed -i '/\/opt\/puppetlabs\/puppet\/bin\/hocon/d' %{SOURCE1}
  sed -i '/\/opt\/puppetlabs\/puppet\/bin\/rmsgcat/d' %{SOURCE1}
  sed -i '/\/opt\/puppetlabs\/puppet\/bin\/rmsgfmt/d' %{SOURCE1}
  sed -i '/\/opt\/puppetlabs\/puppet\/bin\/rmsginit/d' %{SOURCE1}
  sed -i '/\/opt\/puppetlabs\/puppet\/bin\/rmsgmerge/d' %{SOURCE1}
  sed -i '/\/opt\/puppetlabs\/puppet\/bin\/rxgettext/d' %{SOURCE1}
  sed -i '/\/opt\/puppetlabs\/puppet\/bin\/stompcat/d' %{SOURCE1}
  sed -i '/\/opt\/puppetlabs\/puppet\/lib\/ruby\/vendor_ruby\/facter.jar/d' %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/libgcc_s.so" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/libgcc_s.so.1" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/libssp.so" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/libssp.so.0" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/libssp.so.0.0.0" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/libstdc++.so" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/libstdc++.so.6" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/libstdc++.so.6.0.22" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/bin/catstomp" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/bin/hocon" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/bin/rmsgcat" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/bin/rmsgfmt" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/bin/rmsginit" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/bin/rmsgmerge" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/bin/rxgettext" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/bin/stompcat" >> %{SOURCE1}
  echo "/opt/puppetlabs/puppet/share/doc/rbconfig.rb" >> %{SOURCE1}
fi

if [ %{rhel} = 5 ]; then
  sed -i '/\/opt\/puppetlabs\/puppet\/lib\/ruby\/vendor_ruby\/facter.jar/d' %{SOURCE1}
  sed -i '/\/opt\/puppetlabs\/puppet\/ssl\/puppet-cacerts/d' %{SOURCE1}
fi

%build

%clean

%install
test -d /opt/freeware/bin && export PATH="/opt/freeware/bin:${PATH}"
rm -rf %{buildroot}
install -d %{buildroot}

cd %{_builddir}/%{name}-%{version}/el%{rhel}/%{_arch}/
cp -r * %{buildroot}

# Copy each directory into place. Because empty directories won't make it into.
 if [ -d opt/puppetlabs ]; then
 install -d %{buildroot}/opt
 cp -pr opt/puppetlabs %{buildroot}/opt
 else
 install -d %{buildroot}/opt/puppetlabs
 fi
 if [ -d opt/puppetlabs/puppet ]; then
 install -d %{buildroot}/opt/puppetlabs
 cp -pr opt/puppetlabs/puppet %{buildroot}/opt/puppetlabs
 else
 install -d %{buildroot}/opt/puppetlabs/puppet
 fi
 if [ -d etc/puppetlabs ]; then
 install -d %{buildroot}/etc
 cp -pr etc/puppetlabs %{buildroot}/etc
 else
 install -d %{buildroot}/etc/puppetlabs
 fi
 if [ -d opt/puppetlabs/bin ]; then
 install -d %{buildroot}/opt/puppetlabs
 cp -pr opt/puppetlabs/bin %{buildroot}/opt/puppetlabs
 else
 install -d %{buildroot}/opt/puppetlabs/bin
 fi
 if [ -d var/log/puppetlabs ]; then
 install -d %{buildroot}/var/log
 cp -pr var/log/puppetlabs %{buildroot}/var/log
 else
 install -d %{buildroot}/var/log/puppetlabs
 fi
 if [ -d var/run/puppetlabs ]; then
 install -d %{buildroot}/var/run
 cp -pr var/run/puppetlabs %{buildroot}/var/run
 else
 install -d %{buildroot}/var/run/puppetlabs
 fi
 if [ -d opt/puppetlabs/puppet/cache ]; then
 install -d %{buildroot}/opt/puppetlabs/puppet
 cp -pr opt/puppetlabs/puppet/cache %{buildroot}/opt/puppetlabs/puppet
 else
 install -d %{buildroot}/opt/puppetlabs/puppet/cache
 fi
 if [ -d etc/puppetlabs/puppet ]; then
 install -d %{buildroot}/etc/puppetlabs
 cp -pr etc/puppetlabs/puppet %{buildroot}/etc/puppetlabs
 else
 install -d %{buildroot}/etc/puppetlabs/puppet
 fi
 if [ -d opt/puppetlabs/puppet/share/locale ]; then
 install -d %{buildroot}/opt/puppetlabs/puppet/share
 cp -pr opt/puppetlabs/puppet/share/locale %{buildroot}/opt/puppetlabs/puppet/share
 else
 install -d %{buildroot}/opt/puppetlabs/puppet/share/locale
 fi
 if [ -d etc/puppetlabs/code ]; then
 install -d %{buildroot}/etc/puppetlabs
 cp -pr etc/puppetlabs/code %{buildroot}/etc/puppetlabs
 else
 install -d %{buildroot}/etc/puppetlabs/code
 fi
 if [ -d etc/puppetlabs/code/modules ]; then
 install -d %{buildroot}/etc/puppetlabs/code
 cp -pr etc/puppetlabs/code/modules %{buildroot}/etc/puppetlabs/code
 else
 install -d %{buildroot}/etc/puppetlabs/code/modules
 fi
 if [ -d opt/puppetlabs/puppet/modules ]; then
 install -d %{buildroot}/opt/puppetlabs/puppet
 cp -pr opt/puppetlabs/puppet/modules %{buildroot}/opt/puppetlabs/puppet
 else
 install -d %{buildroot}/opt/puppetlabs/puppet/modules
 fi
 if [ -d etc/puppetlabs/code/environments ]; then
 install -d %{buildroot}/etc/puppetlabs/code
 cp -pr etc/puppetlabs/code/environments %{buildroot}/etc/puppetlabs/code
 else
 install -d %{buildroot}/etc/puppetlabs/code/environments
 fi
 if [ -d etc/puppetlabs/code/environments/production ]; then
 install -d %{buildroot}/etc/puppetlabs/code/environments
 cp -pr etc/puppetlabs/code/environments/production %{buildroot}/etc/puppetlabs/code/environments
 else
 install -d %{buildroot}/etc/puppetlabs/code/environments/production
 fi
 if [ -d etc/puppetlabs/code/environments/production/manifests ]; then
 install -d %{buildroot}/etc/puppetlabs/code/environments/production
 cp -pr etc/puppetlabs/code/environments/production/manifests %{buildroot}/etc/puppetlabs/code/environments/production
 else
 install -d %{buildroot}/etc/puppetlabs/code/environments/production/manifests
 fi
 if [ -d etc/puppetlabs/code/environments/production/modules ]; then
 install -d %{buildroot}/etc/puppetlabs/code/environments/production
 cp -pr etc/puppetlabs/code/environments/production/modules %{buildroot}/etc/puppetlabs/code/environments/production
 else
 install -d %{buildroot}/etc/puppetlabs/code/environments/production/modules
 fi
 if [ -d var/log/puppetlabs/puppet ]; then
 install -d %{buildroot}/var/log/puppetlabs
 cp -pr var/log/puppetlabs/puppet %{buildroot}/var/log/puppetlabs
 else
 install -d %{buildroot}/var/log/puppetlabs/puppet
 fi
 if [ -d opt/puppetlabs/facter/facts.d ]; then
 install -d %{buildroot}/opt/puppetlabs/facter
 cp -pr opt/puppetlabs/facter/facts.d %{buildroot}/opt/puppetlabs/facter
 else
 install -d %{buildroot}/opt/puppetlabs/facter/facts.d
 fi
 if [ -d etc/puppetlabs/code/environments/production/hieradata ]; then
 install -d %{buildroot}/etc/puppetlabs/code/environments/production
 cp -pr etc/puppetlabs/code/environments/production/hieradata %{buildroot}/etc/puppetlabs/code/environments/production
 else
 install -d %{buildroot}/etc/puppetlabs/code/environments/production/hieradata
 fi
 if [ -d etc/puppetlabs/mcollective ]; then
 install -d %{buildroot}/etc/puppetlabs
 cp -pr etc/puppetlabs/mcollective %{buildroot}/etc/puppetlabs
 else
 install -d %{buildroot}/etc/puppetlabs/mcollective
 fi
 if [ -d opt/puppetlabs/mcollective/plugins ]; then
 install -d %{buildroot}/opt/puppetlabs/mcollective
 cp -pr opt/puppetlabs/mcollective/plugins %{buildroot}/opt/puppetlabs/mcollective
 else
 install -d %{buildroot}/opt/puppetlabs/mcollective/plugins
 fi
 if [ -d etc/puppetlabs/pxp-agent ]; then
 install -d %{buildroot}/etc/puppetlabs
 cp -pr etc/puppetlabs/pxp-agent %{buildroot}/etc/puppetlabs
 else
 install -d %{buildroot}/etc/puppetlabs/pxp-agent
 fi
 if [ -d etc/puppetlabs/pxp-agent/modules ]; then
 install -d %{buildroot}/etc/puppetlabs/pxp-agent
 cp -pr etc/puppetlabs/pxp-agent/modules %{buildroot}/etc/puppetlabs/pxp-agent
 else
 install -d %{buildroot}/etc/puppetlabs/pxp-agent/modules
 fi
 if [ -d opt/puppetlabs/pxp-agent/spool ]; then
 install -d %{buildroot}/opt/puppetlabs/pxp-agent
 cp -pr opt/puppetlabs/pxp-agent/spool %{buildroot}/opt/puppetlabs/pxp-agent
 else
 install -d %{buildroot}/opt/puppetlabs/pxp-agent/spool
 fi
 if [ -d var/log/puppetlabs/pxp-agent ]; then
 install -d %{buildroot}/var/log/puppetlabs
 cp -pr var/log/puppetlabs/pxp-agent %{buildroot}/var/log/puppetlabs
 else
 install -d %{buildroot}/var/log/puppetlabs/pxp-agent
 fi

# Copy each of the extra files into place
 install -d %{buildroot}/opt/puppetlabs/puppet
 cp -Rp opt/puppetlabs/puppet/VERSION %{buildroot}/opt/puppetlabs/puppet
 install -d %{buildroot}/opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/specifications
 cp -Rp opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/specifications/puppet.gemspec %{buildroot}/opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/specifications
 install -d %{buildroot}/opt/puppetlabs/bin
 cp -Rp opt/puppetlabs/bin/puppet %{buildroot}/opt/puppetlabs/bin
 install -d %{buildroot}/opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/specifications
 cp -Rp opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/specifications/facter.gemspec %{buildroot}/opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/specifications
 install -d %{buildroot}/opt/puppetlabs/bin
 cp -Rp opt/puppetlabs/bin/facter %{buildroot}/opt/puppetlabs/bin
 install -d %{buildroot}/opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/specifications
 cp -Rp opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/specifications/hiera.gemspec %{buildroot}/opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/specifications
 install -d %{buildroot}/opt/puppetlabs/bin
 cp -Rp opt/puppetlabs/bin/hiera %{buildroot}/opt/puppetlabs/bin
 install -d %{buildroot}/opt/puppetlabs/bin
 cp -Rp opt/puppetlabs/bin/mco %{buildroot}/opt/puppetlabs/bin
 install -d %{buildroot}/opt/puppetlabs/puppet/lib/ruby/vendor_ruby
 cp -Rp opt/puppetlabs/puppet/lib/ruby/vendor_ruby/augeas.rb %{buildroot}/opt/puppetlabs/puppet/lib/ruby/vendor_ruby
 install -d %{buildroot}/opt/puppetlabs/puppet/share/doc/openssl-1.0.2k
 cp -Rp opt/puppetlabs/puppet/share/doc/openssl-1.0.2k/LICENSE %{buildroot}/opt/puppetlabs/puppet/share/doc/openssl-1.0.2k
 install -d %{buildroot}/etc/profile.d
 cp -Rp etc/profile.d/puppet-agent.sh %{buildroot}/etc/profile.d
 install -d %{buildroot}/etc/profile.d
 cp -Rp etc/profile.d/puppet-agent.csh %{buildroot}/etc/profile.d
 install -d %{buildroot}/etc/sysconfig
 cp -Rp etc/sysconfig/puppet %{buildroot}/etc/sysconfig
 install -d %{buildroot}/opt/puppetlabs/puppet/share/vim/puppet-vimfiles/ftdetect
 cp -Rp opt/puppetlabs/puppet/share/vim/puppet-vimfiles/ftdetect/puppet.vim %{buildroot}/opt/puppetlabs/puppet/share/vim/puppet-vimfiles/ftdetect
 install -d %{buildroot}/opt/puppetlabs/puppet/share/vim/puppet-vimfiles/ftplugin
 cp -Rp opt/puppetlabs/puppet/share/vim/puppet-vimfiles/ftplugin/puppet.vim %{buildroot}/opt/puppetlabs/puppet/share/vim/puppet-vimfiles/ftplugin
 install -d %{buildroot}/opt/puppetlabs/puppet/share/vim/puppet-vimfiles/indent
 cp -Rp opt/puppetlabs/puppet/share/vim/puppet-vimfiles/indent/puppet.vim %{buildroot}/opt/puppetlabs/puppet/share/vim/puppet-vimfiles/indent
 install -d %{buildroot}/opt/puppetlabs/puppet/share/vim/puppet-vimfiles/syntax
 cp -Rp opt/puppetlabs/puppet/share/vim/puppet-vimfiles/syntax/puppet.vim %{buildroot}/opt/puppetlabs/puppet/share/vim/puppet-vimfiles/syntax
 install -d %{buildroot}/etc/puppetlabs/puppet
 cp -Rp etc/puppetlabs/puppet/puppet.conf %{buildroot}/etc/puppetlabs/puppet
 install -d %{buildroot}/etc/puppetlabs/puppet
 cp -Rp etc/puppetlabs/puppet/auth.conf %{buildroot}/etc/puppetlabs/puppet
 install -d %{buildroot}/etc/puppetlabs/code/environments/production
 cp -Rp etc/puppetlabs/code/environments/production/environment.conf %{buildroot}/etc/puppetlabs/code/environments/production
 install -d %{buildroot}/etc/puppetlabs/puppet
 cp -Rp etc/puppetlabs/puppet/hiera.yaml %{buildroot}/etc/puppetlabs/puppet
 install -d %{buildroot}/etc/sysconfig
 cp -Rp etc/sysconfig/mcollective %{buildroot}/etc/sysconfig
 install -d %{buildroot}/etc/puppetlabs/mcollective
 cp -Rp etc/puppetlabs/mcollective/client.cfg %{buildroot}/etc/puppetlabs/mcollective
 install -d %{buildroot}/etc/puppetlabs/mcollective
 cp -Rp etc/puppetlabs/mcollective/server.cfg %{buildroot}/etc/puppetlabs/mcollective
 install -d %{buildroot}/etc/puppetlabs/mcollective
 cp -Rp etc/puppetlabs/mcollective/facts.yaml %{buildroot}/etc/puppetlabs/mcollective
 install -d %{buildroot}/etc/logrotate.d
 cp -Rp etc/logrotate.d/mcollective %{buildroot}/etc/logrotate.d
 install -d %{buildroot}/etc/sysconfig
 cp -Rp etc/sysconfig/pxp-agent %{buildroot}/etc/sysconfig
 install -d %{buildroot}/etc/logrotate.d
 cp -Rp etc/logrotate.d/pxp-agent %{buildroot}/etc/logrotate.d

 mkdir -p %{buildroot}/usr/bin
 ln -sv /opt/puppetlabs/bin/puppet %{buildroot}/usr/bin/puppet

%if 0%{?rhel} >= 7
 install -d %{buildroot}/usr/lib/systemd/system
 cp -Rp usr/lib/systemd/system/puppet.service %{buildroot}/usr/lib/systemd/system
 install -d %{buildroot}/usr/lib/systemd/system
 cp -Rp usr/lib/systemd/system/mcollective.service %{buildroot}/usr/lib/systemd/system
 install -d %{buildroot}/usr/lib/systemd/system
 cp -Rp usr/lib/systemd/system/pxp-agent.service %{buildroot}/usr/lib/systemd/system
 install -d %{buildroot}/usr/lib/tmpfiles.d
 cp -Rp usr/lib/tmpfiles.d/puppet-agent.conf %{buildroot}/usr/lib/tmpfiles.d
%else
 install -d %{buildroot}/etc/rc.d/init.d
 cp -Rp etc/rc.d/init.d/puppet %{buildroot}/etc/rc.d/init.d
 install -d %{buildroot}/etc/rc.d/init.d
 cp -Rp etc/rc.d/init.d/mcollective %{buildroot}/etc/rc.d/init.d
 install -d %{buildroot}/etc/rc.d/init.d
 cp -Rp etc/rc.d/init.d/pxp-agent %{buildroot}/etc/rc.d/init.d
%endif

# Here we explicitly remove the directories and files that we list in the
# %files section separately because rpm3 on aix errors on duplicate files in
# the package.
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/opt/puppetlabs$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/opt/puppetlabs/puppet$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/puppetlabs$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/opt/puppetlabs/bin$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/var/log/puppetlabs$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/var/run/puppetlabs$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/opt/puppetlabs/puppet/cache$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/puppetlabs/puppet$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/opt/puppetlabs/puppet/share/locale$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/puppetlabs/code$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/puppetlabs/code/modules$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/opt/puppetlabs/puppet/modules$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/puppetlabs/code/environments$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/puppetlabs/code/environments/production$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/puppetlabs/code/environments/production/manifests$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/puppetlabs/code/environments/production/modules$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/var/log/puppetlabs/puppet$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/opt/puppetlabs/facter/facts.d$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/puppetlabs/code/environments/production/hieradata$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/puppetlabs/mcollective$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/opt/puppetlabs/mcollective/plugins$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/puppetlabs/pxp-agent$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/puppetlabs/pxp-agent/modules$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/opt/puppetlabs/pxp-agent/spool$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/var/log/puppetlabs/pxp-agent$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/opt/puppetlabs/puppet/VERSION$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/specifications/puppet.gemspec$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/opt/puppetlabs/bin/puppet$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/specifications/facter.gemspec$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/opt/puppetlabs/bin/facter$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/specifications/hiera.gemspec$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/opt/puppetlabs/bin/hiera$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/opt/puppetlabs/bin/mco$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/opt/puppetlabs/puppet/lib/ruby/vendor_ruby/augeas.rb$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/opt/puppetlabs/puppet/share/doc/openssl-1.0.2k/LICENSE$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/profile.d/puppet-agent.sh$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/profile.d/puppet-agent.csh$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/sysconfig/puppet$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/opt/puppetlabs/puppet/share/vim/puppet-vimfiles/ftdetect/puppet.vim$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/opt/puppetlabs/puppet/share/vim/puppet-vimfiles/ftplugin/puppet.vim$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/opt/puppetlabs/puppet/share/vim/puppet-vimfiles/indent/puppet.vim$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/opt/puppetlabs/puppet/share/vim/puppet-vimfiles/syntax/puppet.vim$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/puppetlabs/puppet/puppet.conf$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/puppetlabs/puppet/auth.conf$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/puppetlabs/code/environments/production/environment.conf$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/puppetlabs/puppet/hiera.yaml$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/sysconfig/mcollective$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/puppetlabs/mcollective/client.cfg$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/puppetlabs/mcollective/server.cfg$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/puppetlabs/mcollective/facts.yaml$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/logrotate.d/mcollective$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/sysconfig/pxp-agent$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/logrotate.d/pxp-agent$||g' %{SOURCE1}

%if 0%{?rhel} >= 7
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/usr/lib/systemd/system/puppet.service$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/usr/lib/systemd/system/mcollective.service$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/usr/lib/systemd/system/pxp-agent.service$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/usr/lib/tmpfiles.d/puppet-agent.conf$||g' %{SOURCE1}
%else
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/rc.d/init.d/puppet$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/rc.d/init.d/mcollective$||g' %{SOURCE1}
 PATH=/opt/freeware/bin:$PATH sed -i 's|^/etc/rc.d/init.d/pxp-agent$||g' %{SOURCE1}
%endif

# Here we turn all dirs in the file-list into %dir entries to avoid duplicate files
for entry in `cat %{SOURCE1}`; do
 if [ -n "$entry" -a -d "$entry" -a ! -L "$entry" ]; then
 PATH=/opt/freeware/bin:$PATH sed -i "s|^\($entry\)\$|%dir \1|g" %{SOURCE1}
 fi
done

export QA_RPATHS=3

%pre
# Save state so we know later if this is an upgrade or an install
mkdir -p %{_localstatedir}/lib/rpm-state/%{name}
if [ "$1" -eq 1 ] ; then
 touch %{_localstatedir}/lib/rpm-state/%{name}/install
fi
if [ "$1" -gt 1 ] ; then
 touch %{_localstatedir}/lib/rpm-state/%{name}/upgrade
fi

# Run preinstall scripts on install if defined
if [ "$1" -eq 1 ] ; then
 : no preinstall scripts provided
fi

# Run preinstall scripts on upgrade if defined
if [ "$1" -gt 1 ] ; then
 if [ -e /etc/puppetlabs/code/hiera.yaml ]; then cp /etc/puppetlabs/code/hiera.yaml /etc/puppetlabs/code/hiera.yaml.pkg-old; fi
fi

%post
%if 0%{?rhel} >= 7
 # switch based on systemd vs systemv vs smf vs aix
 #
 %systemd_post puppet.service
 # switch based on systemd vs systemv vs smf vs aix
 #
 %systemd_post mcollective.service
 # switch based on systemd vs systemv vs smf vs aix
 #
 %systemd_post pxp-agent.service
%else
  # switch based on systemd vs systemv vs smf vs aix
  #
    chkconfig --add puppet >/dev/null 2>&1 || :
  # switch based on systemd vs systemv vs smf vs aix
  #
    chkconfig --add mcollective >/dev/null 2>&1 || :
  # switch based on systemd vs systemv vs smf vs aix
  #
    chkconfig --add pxp-agent >/dev/null 2>&1 || :
%endif

%postun
# Run post-uninstall scripts on upgrade if defined
if [ "$1" -eq 1 ] ; then
 : no postremove scripts provided
fi

# Run post-uninstall scripts on removal if defined
if [ "$1" -eq 0 ] ; then
 : no postremove scripts provided
fi

%if 0%{?rhel} >= 7
 # switch based on systemd vs systemv vs smf vs aix
 #
 %systemd_postun_with_restart puppet.service
 # switch based on systemd vs systemv vs smf vs aix
 #
 %systemd_postun_with_restart mcollective.service
 # switch based on systemd vs systemv vs smf vs aix
 #
 %systemd_postun_with_restart pxp-agent.service
%else
  if [ "$1" -eq 1 ]; then
    /sbin/service puppet condrestart || :
  fi
# switch based on systemd vs systemv vs smf vs aix
#
  if [ "$1" -eq 1 ]; then
    /sbin/service mcollective condrestart || :
  fi
# switch based on systemd vs systemv vs smf vs aix
#
  if [ "$1" -eq 1 ]; then
    /sbin/service pxp-agent condrestart || :
  fi
%endif

%preun
# Run pre-uninstall scripts on upgrade if defined
if [ "$1" -eq 1 ] ; then
 : no preremove scripts provided
fi

# Run pre-uninstall scripts on removal if defined
if [ "$1" -eq 0 ] ; then
 : no preremove scripts provided
fi

%if 0%{?rhel} >= 7
  %systemd_preun puppet.service
  %systemd_preun mcollective.service
  %systemd_preun pxp-agent.service
%else
  if [ "$1" -eq 0 ]; then
    /sbin/service puppet stop >/dev/null 2>&1 || :
    chkconfig --del puppet || :
  fi
  if [ "$1" -eq 0 ]; then
    /sbin/service mcollective stop >/dev/null 2>&1 || :
    chkconfig --del mcollective || :
  fi
  if [ "$1" -eq 0 ]; then
    /sbin/service pxp-agent stop >/dev/null 2>&1 || :
    chkconfig --del pxp-agent || :
  fi
%endif


%posttrans
# Run post-transaction scripts on install if defined
if [ -e %{_localstatedir}/lib/rpm-state/%{name}/install ] ; then
 : no postinstall scripts provided
 rm %{_localstatedir}/lib/rpm-state/%{name}/install
fi

# Run post-transaction scripts on upgrade if defined
if [ -e %{_localstatedir}/lib/rpm-state/%{name}/upgrade ] ; then
 if [ -e /etc/puppetlabs/code/hiera.yaml.pkg-old ]; then mv /etc/puppetlabs/code/hiera.yaml.pkg-old /etc/puppetlabs/code/hiera.yaml; fi
 rm %{_localstatedir}/lib/rpm-state/%{name}/upgrade
fi

%files -f %{SOURCE1}
%doc /usr/share/doc/puppet-agent-1.10.9/bill-of-materials
%defattr(-, root, root, 0755)
%dir %attr(-, -, -) /opt/puppetlabs
%dir %attr(-, -, -) /opt/puppetlabs/puppet
%dir %attr(-, -, -) /etc/puppetlabs
%dir %attr(-, -, -) /opt/puppetlabs/bin
%dir %attr(-, -, -) /var/log/puppetlabs
%dir %attr(-, -, -) /var/run/puppetlabs
%dir %attr(0750, -, -) /opt/puppetlabs/puppet/cache
%dir %attr(-, -, -) /etc/puppetlabs/puppet
%dir %attr(-, -, -) /opt/puppetlabs/puppet/share/locale
%dir %attr(-, -, -) /etc/puppetlabs/code
%dir %attr(-, -, -) /etc/puppetlabs/code/modules
%dir %attr(-, -, -) /opt/puppetlabs/puppet/modules
%dir %attr(-, -, -) /etc/puppetlabs/code/environments
%dir %attr(-, -, -) /etc/puppetlabs/code/environments/production
%dir %attr(-, -, -) /etc/puppetlabs/code/environments/production/manifests
%dir %attr(-, -, -) /etc/puppetlabs/code/environments/production/modules
%dir %attr(0750, -, -) /var/log/puppetlabs/puppet
%dir %attr(-, -, -) /opt/puppetlabs/facter/facts.d
%dir %attr(-, -, -) /etc/puppetlabs/code/environments/production/hieradata
%dir %attr(-, -, -) /etc/puppetlabs/mcollective
%dir %attr(-, -, -) /opt/puppetlabs/mcollective/plugins
%dir %attr(-, -, -) /etc/puppetlabs/pxp-agent
%dir %attr(-, -, -) /etc/puppetlabs/pxp-agent/modules
%dir %attr(-, -, -) /opt/puppetlabs/pxp-agent/spool
%dir %attr(-, -, -) /var/log/puppetlabs/pxp-agent
%config(noreplace) %attr(-, -, -) /etc/sysconfig/puppet
%config(noreplace) %attr(0644, -, -) /opt/puppetlabs/puppet/share/vim/puppet-vimfiles/ftdetect/puppet.vim
%config(noreplace) %attr(0644, -, -) /opt/puppetlabs/puppet/share/vim/puppet-vimfiles/ftplugin/puppet.vim
%config(noreplace) %attr(0644, -, -) /opt/puppetlabs/puppet/share/vim/puppet-vimfiles/indent/puppet.vim
%config(noreplace) %attr(0644, -, -) /opt/puppetlabs/puppet/share/vim/puppet-vimfiles/syntax/puppet.vim
%config(noreplace) %attr(-, -, -) /etc/puppetlabs/puppet/puppet.conf
%config(noreplace) %attr(-, -, -) /etc/puppetlabs/puppet/auth.conf
%config(noreplace) %attr(0644, -, -) /etc/puppetlabs/code/environments/production/environment.conf
%config(noreplace) %attr(-, -, -) /etc/puppetlabs/puppet/hiera.yaml
%config(noreplace) %attr(-, -, -) /etc/sysconfig/mcollective
%config(noreplace) %attr(-, -, -) /etc/puppetlabs/mcollective/client.cfg
%config(noreplace) %attr(-, -, -) /etc/puppetlabs/mcollective/server.cfg
%config(noreplace) %attr(-, -, -) /etc/puppetlabs/mcollective/facts.yaml
%config(noreplace) %attr(-, -, -) /etc/logrotate.d/mcollective
%config(noreplace) %attr(-, -, -) /etc/sysconfig/pxp-agent
%config(noreplace) %attr(0644, -, -) /etc/logrotate.d/pxp-agent
%attr(-, -, -) /opt/puppetlabs/puppet/VERSION
%attr(0644, -, -) /opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/specifications/puppet.gemspec
%attr(-, -, -) /opt/puppetlabs/bin/puppet
%attr(0644, -, -) /opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/specifications/facter.gemspec
%attr(-, -, -) /opt/puppetlabs/bin/facter
%attr(0644, -, -) /opt/puppetlabs/puppet/lib/ruby/gems/2.1.0/specifications/hiera.gemspec
%attr(-, -, -) /opt/puppetlabs/bin/hiera
%attr(-, -, -) /opt/puppetlabs/bin/mco
%attr(0644, -, -) /opt/puppetlabs/puppet/lib/ruby/vendor_ruby/augeas.rb
%attr(0644, -, -) /opt/puppetlabs/puppet/share/doc/openssl-1.0.2k/LICENSE
%attr(0644, -, -) /etc/profile.d/puppet-agent.sh
%attr(0644, -, -) /etc/profile.d/puppet-agent.csh
%{_bindir}/puppet

%if 0%{?rhel} >= 7
  %attr(0644, root, root) /usr/lib/systemd/system/mcollective.service
  %attr(0644, root, root) /usr/lib/systemd/system/puppet.service
  %attr(0644, root, root) /usr/lib/systemd/system/pxp-agent.service
  %config(noreplace) %attr(0644, -, -) /usr/lib/tmpfiles.d/puppet-agent.conf
%else
  %attr(0755, root, root) /etc/rc.d/init.d/mcollective
  %attr(0755, root, root) /etc/rc.d/init.d/puppet
  %attr(0755, root, root) /etc/rc.d/init.d/pxp-agent
%endif

%changelog
* Thu Dec 05 2019 Ben Morrice <ben.morrice@cern.ch> - 1.10.10-1
 - add basic support for el8
* Wed Jan 17 2018 Eric D Helms <ehelms@redhat.com> - 1.10.9-1
- Build for 1.10.9-1
- Add symlink for puppet to /usr/bin

* Fri Sep 22 2017 Puppet Labs <info@puppetlabs.com> - 1.10.7-1
- Build for 1.10.7-1
